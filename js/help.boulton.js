(function ($) {
  Drupal.behaviors.boultonHelp = {
    attach: function (context) {
      var helpRegion, helpLabel;
      $('#help').each(function () {
        var $self = $(this).addClass('expandable');
        helpRegion = $self.find('.region-help').slideUp(0);
        helpLabel = $self.find('h2.label');
        helpLabel.click(function () {
          helpRegion.slideToggle('fast');
          $self.toggleClass('expanded');
        });
      });
    }
  };
})(jQuery);