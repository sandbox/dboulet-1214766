<?php
/**
 * @file
 * Main page template for the Boulton theme.
 */
?>
<?php if ($page['help']): ?>
  <div id="help" class="clearfix">
    <h2 class="label"><?php print t('Help'); ?></h2>
    <?php print render($page['help']); ?>
  </div>
<?php endif; ?>

  <div id="branding" class="clearfix">
    <?php print $breadcrumb; ?>
    <?php print render($title_prefix); ?>
    <?php if ($title): ?>
      <h1 class="page-title"><?php print $title; ?></h1>
    <?php endif; ?>
    <?php print render($title_suffix); ?>
    <?php if ($action_links): ?>
      <ul class="action-links">
        <?php print render($action_links); ?>
      </ul>
    <?php endif; ?>
    <?php print render($primary_local_tasks); ?>
  </div>

  <div id="page">
    <?php $secondary_local_tasks_rendered = render($secondary_local_tasks); ?>
    <?php if (!empty($secondary_local_tasks_rendered)): ?>
      <div class="tabs-secondary clearfix"><ul class="tabs secondary"><?php print $secondary_local_tasks_rendered; ?></ul></div>
    <?php endif; ?>

    <div id="content" class="clearfix">
      <div class="element-invisible"><a id="main-content"></a></div>
      <?php if ($messages): ?>
        <div id="console" class="clearfix"><?php print $messages; ?></div>
      <?php endif; ?>
      <?php print render($page['content']); ?>
    </div>

    <div id="footer">
      <?php print $feed_icons; ?>
    </div>

  </div>
