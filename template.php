<?php
/**
 * @file
 * Theme functions for Boulton theme.
 */

/**
 * Preprocess the HTML template.
 */
function boulton_preprocess_html(&$variables) {
  // Add in body classes.
  $color = theme_get_setting('boulton_header_color');
  $variables['classes_array'][] = 'color-scheme-' . $color;

  // Force iOS to size the site correctly
  $element = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'name' => 'viewport',
      'content' => 'width=device-width; initial-scale=1.0, maximum-scale=1.0',
    ),
  );
  drupal_add_html_head($element, 'boulton_ios_meta_tags');

  // Add conditional CSS for IE8 and below.
  drupal_add_css(path_to_theme() . '/css/ie.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'lte IE 8', '!IE' => FALSE), 'weight' => 999, 'preprocess' => FALSE));
  // Add conditional CSS for IE7 and below.
  drupal_add_css(path_to_theme() . '/css/ie7.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'lte IE 7', '!IE' => FALSE), 'weight' => 999, 'preprocess' => FALSE));
  // Add conditional CSS for IE6.
  drupal_add_css(path_to_theme() . '/css/ie6.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'lte IE 6', '!IE' => FALSE), 'weight' => 999, 'preprocess' => FALSE));
}

/**
 * Theme breacrumbs
 */
function boulton_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];

  if (!empty($breadcrumb)) {
    // Provide a navigational heading to give context for breadcrumb links to
    // screen-reader users. Make the heading invisible with .element-invisible.
    $output = '<h2 class="element-invisible">' . t('You are here') . '</h2>';

    $site_name = variable_get('site_name', t('Home'));
    $breadcrumb[0] = '<a href="' . base_path() . '">' . $site_name . '</a>';
    $output .= '<div class="breadcrumb">' . implode(' › ', $breadcrumb) . '</div>';
    return $output;
  }
}

/**
 * Returns HTML for a confirmation form.
 */
function boulton_confirm_form($variables) {
  $form = $variables['form'];
  $form['actions']['cancel']['#prefix'] = '<span class="seperator">' . t('or') . '</span> ';
  return drupal_render_children($form);
}

/**
 * Returns HTML for a system settings form.
 */
function boulton_system_settings_form($variables) {
  $form = $variables['form'];
  if (!isset($form['actions']['cancel'])) {
    $form['actions']['cancel'] = array(
      '#type' => 'link',
      '#title' => t('Cancel'),
      '#href' => boulton_get_cancel_destination(),
      '#weight' => 100,
    );
  }
  $form['actions']['cancel']['#prefix'] = '<span class="seperator">' . t('or') . '</span> ';
  return drupal_render_children($form);
}

/**
 * Implements hook_form_alter().
 */
function boulton_form_alter(&$form, &$form_state, $form_id) {
  // Add cancel link to node forms.
  if (!empty($form['#node_edit_form'])) {
    if (!isset($form['actions']['cancel'])) {
      $form['actions']['cancel'] = array(
        '#type' => 'link',
        '#title' => t('Cancel'),
        '#href' => boulton_get_cancel_destination(),
        '#weight' => 100,
      );
    }
  }
  if (isset($form['actions']['cancel'])) {
    $form['actions']['cancel']['#prefix'] = isset($form['actions']['cancel']['#prefix']) ? $form['actions']['cancel']['#prefix'] : '';
    $form['actions']['cancel']['#prefix'] .= '<span class="seperator">' . t('or') . '</span> ';
  }
}

/**
 * Return the appropriate path for form cancel links.
 */
function boulton_get_cancel_destination() {
  $url = array('path' => '<front>');
  if (isset($_REQUEST['destination']) && $_REQUEST['destination'] != $_GET['q']) {
    $url = parse_url(urldecode($_REQUEST['destination']));
  }
  else {
    switch (arg(0)) {
      case 'admin':
        if (user_access('access administration pages')) {
          $url['path'] = 'admin/' . arg(1);
        }
        break;
      case 'node':
        if (arg(1) == 'add') {
          $url['path'] = 'node/add';
        }
        elseif (is_numeric(arg(1)) && arg(2) == 'edit') {
          $url['path'] = 'node/' . arg(1);
        }
        break;
    }
  }
  return $url['path'];
}

/**
 * Insert or modify variables for the node add list page.
 *
 * We add classes to each link so that we can style them with icons.
 * TODO Make icons configurable?
 */
function boulton_preprocess_node_add_list(&$variables) {
  $content = $variables['content'];
  if ($content) {
    foreach ($content as &$item) {
      $item['localized_options']['attributes']['class'] = array(drupal_html_class('link-' . $item['href']));
    }
  }
  $variables['content'] = $content;
};

/**
 * Override or insert variables into the views template.
 */
function boulton_preprocess_views_view(&$variables) {
  $view = $variables['view'];
  // Check to see if we are displaying exposed form as links
  $exposed_form = $view->display[$view->current_display]->handler->get_option('exposed_form');
  if ($exposed_form['type'] == 'better_exposed_filters' && isset($exposed_form['options']['bef'])) {
    if (is_array($exposed_form['options']['bef'])) {
      foreach ($exposed_form['options']['bef'] as $key => $value) {
        $variables['classes_array'][] = 	drupal_html_class('view-exposed-filters-' . $value['bef_format']);
      }
    }
  }
}

/**
 * Override or insert variables into the views table template.
 */
function boulton_preprocess_views_view_table(&$variables) {
  _boulton_preprocess_views_table($variables);
}

function boulton_preprocess_draggableviews_view_draggabletable(&$variables) {
  _boulton_preprocess_views_table($variables);
}

/**
 * Preprocess a views table.
 */
function _boulton_preprocess_views_table(&$variables) {
  global $result_count;
  $view = $variables['view'];
  $id = $view->name . '-' . $view->current_display;
  if (!isset($result_count[$id]) || !is_int($result_count[$id])) {
    $result_count[$id] = 0;
  }
  $result = $view->result;
  foreach ($variables['rows'] as $index => $data) {
    $classes = is_array($variables['row_classes'][$index]) ? $variables['row_classes'][$index] : array();
    // Node language class
    if (isset($result[$result_count[$id]]->node_language) && !empty($result[$result_count[$id]]->node_language)) {
      $classes[] = 'node-lang-' . check_plain($result[$result_count[$id]]->node_language);
    }
    // Node status class
    if (isset($result[$result_count[$id]]->node_status)) {
      switch ($result[$result_count[$id]]->node_status) {
        case "1":
          $classes[] = 'node-published';
          break;
        case "0":
          $classes[] = 'node-unpublished';
          break;
      }
    }
    $variables['row_classes'][$index] = $classes;
    $result_count[$id]++;
  }
}

/**
 * Returns HTML for an individual form element.
 * Combine multiple values into a table with drag-n-drop reordering.
 *
 * @see theme_field_multiple_value_form()
 */
function boulton_field_multiple_value_form($variables) {
  $element = $variables['element'];
  $output = '';

  if ($element['#cardinality'] > 1 || $element['#cardinality'] == FIELD_CARDINALITY_UNLIMITED) {
    $table_id = drupal_html_id($element['#field_name'] . '_values');
    $order_class = $element['#field_name'] . '-delta-order';
    $required = !empty($element['#required']) ? '<span class="form-required" title="' . t('This field is required. ') . '">*</span>' : '';

    $header = array(
      array(
        'data' => '<label>' . t('!title !required', array('!title' => $element['#title'], '!required' => $required)) . "</label>",
        'colspan' => 2,
        'class' => array('field-label'),
      ),
      t('Order'),
    );
    $rows = array();

    // Sort items according to '_weight' (needed when the form comes back after
    // preview or failed validation)
    $items = array();
    foreach (element_children($element) as $key) {
      if ($key === 'add_more') {
        $add_more_button = &$element[$key];
        // Shorten the text display for the button.
        $add_more_button['#value'] = $add_more_button['#value'] == t('Add another item') ? t('Add another') : $add_more_button['#value'];
      }
      else {
        $items[] = &$element[$key];
      }
    }
    usort($items, '_field_sort_items_value_helper');

    // Add the items as table rows.
    foreach ($items as $key => $item) {
      $item['_weight']['#attributes']['class'] = array($order_class);
      $delta_element = drupal_render($item['_weight']);
      $cells = array(
        array(
          'data' => '',
          'class' => array('field-multiple-drag'),
        ),
        drupal_render($item),
        array(
          'data' => $delta_element,
          'class' => array('delta-order'),
        ),
      );
      $rows[] = array(
        'data' => $cells,
        'class' => array('draggable'),
      );
    }

    $output = '<div class="form-item field-multiple-table-wrapper">';
    $output .= theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => $table_id, 'class' => array('field-multiple-table rows-count-' . count($rows)))));
    $output .= '<div class="field-add-more-wrapper clearfix">' . drupal_render($add_more_button) . '</div>';
    $output .= $element['#description'] ? '<div class="description">' . $element['#description'] . '</div>' : '';
    $output .= '</div>';

    drupal_add_tabledrag($table_id, 'order', 'sibling', $order_class);
  }
  else {
    foreach (element_children($element) as $key) {
      $output .= drupal_render($element[$key]);
    }
  }

  return $output;
}

/**
 * Preprocess theme_select_as_links().
 *
 * @see theme_select_as_links().
 */
function boulton_preprocess_select_as_links(&$variables) {
  $element = $variables['element'];
  $name = $element['#name'];

  // Collect selected values so we can properly style the links later
  $selected_options = array();
  if (empty($element['#value'])) {
    if (!empty($element['#default_values'])) {
      $selected_options[] = $element['#default_values'];
    }
  }
  else {
    $selected_options[] = $element['#value'];
  }

  // Add to the selected options specified by Views whatever options are in the
  // URL query string, but only for this filter
  $urllist = parse_url(request_uri());
  if (isset($urllist['query'])) {
    $query = array();
    parse_str(urldecode($urllist['query']), $query);
    foreach ($query as $key => $value) {
      if ($key != $name) {
        continue;
      }
      if (is_array($value)) {
        // This filter allows multiple selections, so put each one on the selected_options array
        foreach ($value as $option) {
          $selected_options[] = $option;
        }
      }
      else {
        $selected_options[] = $value;
      }
    }
  }
  $variables['selected_options'] = $selected_options;
}

/**
 * Themes a select drop-down as a collection of links
 *
 * Requires Better Exposed Filters module.
 *
 * @see theme_select(), http://api.drupal.org/api/function/theme_select/6
 * @param array $variables - An array of arrays, the 'element' item holds the properties of the element.
 *                      Properties used: title, value, options, description, name
 * @return HTML string representing the form element.
 */
function boulton_select_as_links($variables) {
  $element = $variables['element'];

  $output = '';
  $name = $element['#name'];
  $selected_options = $variables['selected_options'];

  // Go through each filter option and build the appropriate link or plain text
  foreach ($element['#options'] as $option => $elem) {
    // Check for Taxonomy-based filters
    if (is_object($elem)) {
      list($option, $elem) = each(array_slice($elem->option, 0, 1, TRUE));
    }

    // Check for optgroups. Put subelements in the $element_set array and add a group heading.
    // Otherwise, just add the element to the set
    $element_set = array();
    if (is_array($elem)) {
      $element_set = $elem;
    }
    else {
      $element_set[$option] = $elem;
    }

    $links = array();
    $multiple = !empty($element['#multiple']);

    foreach ($element_set as $key => $value) {
      // Custom ID for each link based on the <select>'s original ID
      $id = drupal_html_id($element['#id'] . '-' . $key);
      if (array_search($key, $selected_options) === FALSE) {
        $link = l($value, bef_replace_query_string_arg($name, $key, $multiple));
        $output .= '<li id="' . $id . '" class="views-bef-link">' . $link . '</li>';
      }
      else {
        // Selected value is output without a link
        $output .= '<li id="' . $id . '" class="views-bef-link bef-select-as-links-selected">' . $value . '</li>';
      }
    }
  }

  $description = isset($element['#description']) ? $element['#description'] : '';

  return '<ul class="bef-select-as-links">' . $output . $description . '</ul>';
}
