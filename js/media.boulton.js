(function ($) {

/**
 * Functionality for the thumbnail display
 */
Drupal.behaviors.boultonMediaAdmin = {
  attach: function (context) {
    if ($('body').is('.page-admin-content-media') && $('.media-display-thumbnails', '#media-admin').length != 0) {
      // Change the default click behaviour. By default, clicking the thumbnail selects it only
      // if the target is not the image.
      // TODO Remove this if ever this changes in Media module.
      if ($('#edit-operation option[value=edit]').length > 0) {
        $('.media-item').unbind('click').bind('click', function (e) {
          var checkbox = $(this).parent().find(':checkbox');
          if (checkbox.is(':checked')) {
            checkbox.attr('checked', false).change();
          } else {
            checkbox.attr('checked', true).change();
          }
        }).find('a').attr('title', '').bind('click', function (e) {
          e.preventDefault();
        });
      }
    }
  }
};
})(jQuery);
