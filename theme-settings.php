<?php
/**
 * @file
 * Theme settings for Boulton theme.
 */

/**
 * Implements hook_form_system_theme_settings_alter().
 */
 function boulton_form_system_theme_settings_alter(&$form, &$form_state) {
  $form['custom'] = array(
    '#type' => 'fieldset',
    '#title' => t('Customization'),
  );
  $form['custom']['boulton_header_color'] = array(
    '#type' => 'select',
    '#title' => t('Header Color'),
    '#default_value' =>theme_get_setting('boulton_header_color'),
    '#options' => array(
      'default' => t('Seven default'),
      'blue' => t('Baby Bluey'),
      'dark' => t('Dark'),
      'green' => t('Minty'),
    ),
    '#description' => t('Choose the color of the header at the top of the page.'),
  );
 }

